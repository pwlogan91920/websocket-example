import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import socket

class Client:
    name = ""
    mode = "System"

    def __init__(self, newSock):
        super()
        self.sock = newSock

users = []

class WSHandler(tornado.websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        print("Socket Opened")
        users.append(Client(self))
        self.write_message(u"<li class='system'>Welcome to the User Experience Instant Messenger!</li>")
        self.write_message(u"<li class='system'>What is your name?</li>")

    def on_message(self, message):
        print("Message Received")
        print(self)
        user = self.find_user(self)
        if user.mode is "System":
            user.name = message
            user.mode = "Public"
            self.broadcast("System","Welcome " + message + "!")
            self.write_message(u"<li class='system'>There are currently "+ str(len(users)) +" users online!</li>")
        else:
            self.broadcast(user.name, message)
        
    def on_close(self):
        user = self.find_user(self)
        users.remove(user)
        print("Socket Closed")
        self.broadcast("System","Goodbye " + user.name + ".")

    def find_user(self, sock):
        for user in users:
            if sock is user.sock:
                return user
    
    def broadcast(self, sender, message):
        for user in users:
            if sender is "System" and user.mode is not "System":
                user.sock.write_message(u"<li class='system'>" + message + "</li>")
            elif sender is user.name:
                user.sock.write_message(u"<li class='self'><span>" + sender + ":&nbsp;</span>" + message + "</li>")
            elif user.mode is not "System":
                user.sock.write_message(u"<li class='user'><span>" + sender + ":&nbsp;</span>" + message + "</li>")

application = tornado.web.Application([
    (r'/ws', WSHandler)
])

if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(1971)
    myIP = socket.gethostbyname(socket.gethostname())
    print('=== Started at ', myIP, ' ===')
    tornado.ioloop.IOLoop.instance().start()