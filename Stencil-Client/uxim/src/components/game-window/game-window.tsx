import { Component, Element } from '@stencil/core';

@Component({
  tag: 'game-window',
  styleUrl: 'game-window.css',
  shadow: true,
})

export class GameWindow {
    @Element() private element: HTMLElement;

    ENDPOINT = "localhost"
    PORT = 1971
    sock = null
    input = null
    log = null

    componentDidLoad() {
        this.sock = new WebSocket('ws://' + this.ENDPOINT + ":" + this.PORT + "/ws")
        this.sock.onopen = this.connectionOpen
        this.sock.onmessage = (e) => { this.messageRecieved(e, this)}
        this.input = this.element.shadowRoot.querySelector(".chat-input") as HTMLInputElement
        this.log = this.element.shadowRoot.querySelector(".chat-thread") as HTMLUListElement
    }

    connectionOpen(e) {
        console.log(e)
    }

    messageRecieved(e, game) {
        game.log.innerHTML += e.data;
    }

    sendMessage(game) {
        if (game.input.value != "")
        {
            console.log("Send: " + game.input.value)
            game.sock.send(game.input.value)
            game.input.value = ""
        }
    }
    
    render() {
        return (
            <div class="main-window">
                <ul class="chat-thread">
                </ul>
                <input class="chat-input" type="text" autocomplete="false" autofocus="true" onChange={() => this.sendMessage(this)}/>>
            </div>
        )
    }
}